# Generated by Django 3.1.2 on 2020-10-29 10:03

from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Phenotype',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, primary_key=True, serialize=False)),
                ('ipn', models.CharField(blank=True, max_length=40, null=True)),
                ('collab_id', models.CharField(blank=True, max_length=300)),
                ('pn_hash', models.CharField(blank=True, max_length=300)),
                ('cohort', models.CharField(max_length=30)),
                ('qc', models.BooleanField(default=False)),
            ],
            options={
                'db_table': 'phenotype_log',
            },
        ),
        migrations.CreateModel(
            name='Submission',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, primary_key=True, serialize=False)),
                ('source_file', models.FileField(upload_to='')),
                ('study', models.CharField(help_text='The study code for the biobank or submisson', max_length=30)),
                ('site', models.CharField(help_text='The site code for the biobank or submisson', max_length=30)),
                ('cohort', models.CharField(help_text='The cohort that the submission represents', max_length=30)),
                ('submission_title', models.CharField(help_text='a helpful title for the model', max_length=30)),
                ('filename', models.CharField(help_text='File name of the uploaded file', max_length=500)),
                ('validated', models.BooleanField(default=False)),
                ('file_type', models.CharField(choices=[('xlsx', '.xlsx'), ('csv', '.csv'), ('tsv', '.tsv')], max_length=5)),
                ('identifier_column', models.CharField(help_text='The column which contains the collaborator ID or field from the biobank/client which identifies a record', max_length=100)),
                ('domain_index_is_genuity_pn', models.BooleanField(default=False)),
                ('created_date', models.DateTimeField(editable=False)),
                ('updated_date', models.DateTimeField()),
            ],
            options={
                'db_table': 'submission_catalog',
            },
        ),
        migrations.AddIndex(
            model_name='submission',
            index=models.Index(fields=['study'], name='submission__study_3c9686_idx'),
        ),
        migrations.AlterUniqueTogether(
            name='submission',
            unique_together={('study', 'site', 'filename')},
        ),
        migrations.AddField(
            model_name='phenotype',
            name='submission_id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='phenoteke.submission'),
        ),
        migrations.AddIndex(
            model_name='phenotype',
            index=models.Index(fields=['ipn'], name='phenotype_l_ipn_d7cbd9_idx'),
        ),
        migrations.AddIndex(
            model_name='phenotype',
            index=models.Index(fields=['collab_id'], name='phenotype_l_collab__050957_idx'),
        ),
        migrations.AddConstraint(
            model_name='phenotype',
            constraint=models.CheckConstraint(check=models.Q(models.Q(('collab_id__isnull', True), ('pn_hash__isnull', False)), models.Q(('collab_id__isnull', False), ('pn_hash__isnull', True)), _connector='OR'), name='pn_or_collab_id_NOT_NULL'),
        ),
        migrations.AlterUniqueTogether(
            name='phenotype',
            unique_together={('ipn', 'collab_id')},
        ),
    ]
