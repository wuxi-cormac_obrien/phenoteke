from django.urls import path
from phenoteke import views

urlpatterns = [
        path(
            "new_submission",
            views.SubmissionUpload.as_view(),
            name = "submission-upload"
        ),
        path(
            "new_submission/<uuid:uuid>",
            views.SubmissionView.as_view(),
            name = "submission"
        )
    ]
