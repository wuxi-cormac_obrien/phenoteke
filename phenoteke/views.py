from django.contrib.auth.decorators import login_required
from django.views.generic import CreateView, DetailView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy

from phenoteke.models import Submission
from phenoteke.forms import SubmissionUploadForm

# What do I do if there are multiple files?
# What do I do with the file

#class SubmissionUpload(LoginRequiredMixin, CreateView):
class SubmissionUpload(CreateView):
    form_class = SubmissionUploadForm
    login_url = '/auth/login/'
    redirect_field_name = 'redirect_to'  
    success_url = ""
    template_name = "submissions/new.html"

    def form_valid(self, form):
        self.uuid = str(form.instance.uuid)
        return super().form_valid(form)

    def get_success_url(self, *args, **kwargs): 
        return reverse_lazy('submission', kwargs={"uuid": self.uuid})

#class SubmissionView(LoginRequiredMixin, DetailView):
class SubmissionView(DetailView):
    pass
    
