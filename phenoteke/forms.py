from django import forms
from phenoteke.models import Submission


class SubmissionUploadForm(forms.ModelForm):
    class Meta:
        model = Submission
        fields = [
            "study",
            "site",
            "submission_title",
            "source_file",
            "file_type",
            "identifier_column",
            "domain_index_is_genuity_pn",
        ]

    source_file = forms.FileField(
        widget=forms.ClearableFileInput(attrs={"multiple": True})
    )
