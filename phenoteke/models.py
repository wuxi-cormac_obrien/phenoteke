
"""
    Individual Phenotype Records
"""
import uuid

from django.db import models
from django.utils import timezone
from django.core.validators import URLValidator

FILETYPE_CHOICE = [
        ('xlsx','.xlsx'),
        ('csv', '.csv'),
        ('tsv', '.tsv')
        ]

class Submission(models.Model):
    """
        A record of an individual submission

        One of these records should be created for each file submission 
        every time a new file Submission is uploaded.
    """
    class Meta:
        db_table = "submission_catalog"
        unique_together = ["study", "site", "filename"]
        indexes = [
            models.Index(fields=['study'])
        ]


    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4)
    source_file = models.FileField()
    # TODO can these be FKs?
    study = models.CharField(null=False, blank=False, max_length=30, help_text="The study code for the biobank or submisson")
    site = models.CharField(null=False, blank=False, max_length=30, help_text="The site code for the biobank or submisson")
    # TODO can these have One-To-Many?
    cohort = models.CharField(null=False, blank=False, max_length=30, help_text="The cohort that the submission represents")
    submission_title = models.CharField(null=False,  blank=False, max_length=30, help_text="a helpful title for the model")
    filename = models.CharField(null=False, blank=False, max_length=500, help_text="File name of the uploaded file")
    validated = models.BooleanField(default=False)
    file_type = models.CharField(max_length=5, choices=FILETYPE_CHOICE)
    # I.e. Collab_id or PN 
    identifier_column = models.CharField(max_length=100, blank=False, null=False, help_text="The column which contains the collaborator ID or field from the biobank/client which identifies a record")
    domain_index_is_genuity_pn = models.BooleanField(default=False)
    created_date = models.DateTimeField(editable=False, auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)

    def __repr__(self):
        return '<Submission: %r>' % self.submission_title


class Phenotype(models.Model):
    """
        Individual row corresponds to an individual Phenotype Record.

        This should map to a row of a Phenotype submission file and in turn map to an
        iPN.
        Question.... Does there be only one IPN in this table????

        Eithe way a record MUST HAVE either a collab_id or a PN
    """

    class Meta:
        db_table = "phenotype_log"
        unique_together = [
            "ipn",
            "collab_id",
        ]  # Assuming IPN to Collab ID could be non unique.
        # Needs to have an hashed PN or a Collab_id
        constraints = [
            models.CheckConstraint(
                name="pn_or_collab_id_NOT_NULL",
                check=models.Q(pn_hash__isnull=False, collab_id__isnull=True)
                | models.Q(pn_hash__isnull=True, collab_id__isnull=False),
            )
        ]
        indexes = [
            models.Index(fields=["ipn",]), 
            models.Index(fields=["collab_id",])        
        ]

    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4)
    ipn = models.CharField(max_length=40,  null=True, blank=True)
    collab_id = models.CharField(
        max_length=300,  blank=True
    )  # Should I hash this?
    pn_hash = models.CharField(max_length=300,  blank=True)
    # Most recent submission ID
    submission_id = models.ForeignKey("Submission", on_delete=models.CASCADE)  #
    cohort = models.CharField(
        max_length=30, null=False, blank=False
    )  # This should really be a FK to something
    qc = models.BooleanField(default=False)
    # Do we still need these?
    # complete = models.BooleanField(default=False))
    # valid = models.BooleanField(default=False))
