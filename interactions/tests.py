from django.test import TestCase
from interactions.backends import LDAPBackend

# Create your tests here.
class TestLDAPBackend(TestCase):

    def test_ldap_authentication(self):
        user = "cormac.obrien"
        password = "c@kes@nd@l4"
        backend = LDAPBackend()
        backend.authenticate(username=user, password=password)
