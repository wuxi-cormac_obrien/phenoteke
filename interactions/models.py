from django.db import models
from django.contrib.auth.models import (
    AbstractBaseUser,
    PermissionsMixin,
    BaseUserManager,
)


class LDAPUserManager(BaseUserManager):

    def _process_user_attrs(self, attrs):
        """
            Just clean up some data from LDAP
        """
        return {
            "guid": attrs["objectGUID"].replace("{", "").replace("}", ""),
            "username": attrs["sAMAccountName"],
            "display_name": attrs["displayName"],
            "given_name": attrs["givenName"],
            "mail": attrs["mail"],
        }


    def update_or_create_user(self, **attrs):
        """
            When we get a user from AD return it, otherwise create a new one
        """
        # TODO get the groups
        created = False
        usr_attrs = self._process_user_attrs(attrs)
        try:
            guid = usr_attrs['guid']
            obj = LDAPUser.objects.get(guid=guid)
            del usr_attrs['guid']
            obj.update(**attrs)
            
        except LDAPUser.DoesNotExist as e:
            created=True
            obj = LDAPUser(**usr_attrs)
        obj.save()
        return created, obj 

class LDAPUser(AbstractBaseUser, PermissionsMixin):
    """We don't want a lot of what Django's default user does.
    We will just create the user based on the LDAP login and work from there
    """

    guid = models.UUIDField(primary_key=True)
    mail = models.EmailField(blank=False)
    given_name = models.CharField(max_length=500, null=False, blank=False)
    username = models.CharField(max_length=500, null=False, blank=False, unique=True)
    display_name = models.CharField(max_length=500, null=False, blank=False)
    is_superuser = models.BooleanField("is a superuser", default=False)

    USERNAME_FIELD = "username"
    objects = LDAPUserManager()
