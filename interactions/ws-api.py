"""
   Interface for calling Mikes PN API.
    Some reimplementation has taken place to make this Python 3 compatible
    and use PyCryptdome instead of pyCrypto as it is better supported.
"""
import pprint
import time
from urllib.parse import urlparse
import re
import hashlib
import requests
import json

import base64

from Crypto import Random
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad, unpad

class WSApiMessage(Exception):
    """
        Catch a Status 0 error from Mikes API
    """

class WSAPI:
    timeout=30
    key = ''
    device_id = ''
    timestamp = ''
    session_id = ''
    currentModule = ''
    userid = ''
    host = ''
    url = ''
    params = {}
    qstring = ''
    sig = ''
    useragent = ''
    application = ''
    retmessage = ''
    retvalues = ''
    retstatus = ''

    def __init__(self, key):
        self.key_str = key
        self.key_bytes = key[:32].encode('utf-8')
        return

    def config(self, config):
        """Load configs from a dict which ideally would come via settings from the environment 
        """
        for key, value in config.items():
            setattr(self, key, value)
        return


    def calc_signature(self):

        signaturestring = "POST" + self.device_id + self.timestamp + self.url + self.key_str
        sigBase64 = base64.b64encode(signaturestring.encode('utf-8'))
        hash_object = hashlib.sha1(sigBase64)
        signature = hash_object.hexdigest()
        esig = self.encrypt(signature)
        return esig

    def encrypt(self, raw):

        raw = pad(raw.encode('utf-8'), AES.block_size)
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(self.key_bytes, AES.MODE_CBC, iv)
        enc = cipher.encrypt(iv + raw)
        return base64.urlsafe_b64encode(enc)

    def decrypt(self, enc1):

        enc = base64.urlsafe_b64decode(enc1)
        iv = enc[:16]
        cipher = AES.new(self.key_bytes, AES.MODE_CBC, iv)
        
        return unpad(cipher.decrypt(enc[16:]), AES.block_size).decode()

    def encrypt_query_string(self):

        queryString = {}
        for (key, value) in self.params.items():
            print(key, value)
            cipher = self.encrypt(value)
            queryString[key] = cipher
        self.qstring = queryString

    def call_api(self):
        self.timestamp = str(int(time.time()))
        self.timeout = 100

        self.sig = self.calc_signature()
        self.encrypt_query_string()
        uri = urlparse(self.host + self.url)
        esignature = self.sig

        header = {}
        header.update({'Content-Type': 'application/x-www-form-urlencoded'})
        header.update({'signature': esignature})
        header.update({'Device_ID': self.device_id})
        header.update({'Authorization': esignature})
        header.update({'id': self.device_id})
        header.update({'User-Agent': self.useragent})
        header.update({'timestamp': self.timestamp})
        header.update({'session_id': self.session_id})
        header.update({'current_form': self.application})
        header.update({'user_id': self.userid})
        header.update({'CRYTO_VER': "2"})

        try:
            response = requests.post(self.host + self.url, data=self.qstring, headers=header, timeout=self.timeout)
            if response.status_code != 200:
                self.retstatus = 0
                self.retmessage = response.reason
                self.retvalues = response.status_code
            else:
                returnmessage = json.loads(response.content)
                self.retstatus = returnmessage['status']
                self.retmessage = returnmessage['message']
                self.retvalues = returnmessage['values']

                if self.retstatus == "3":  # response is encrypted
                    clearJSON = json.loads(self.decrypt(base64.b64decode(returnmessage['values']+"======")))
                    self.retstatus = clearJSON['status']
                    self.retmessage = clearJSON['message']
                    self.retvalues = clearJSON['values']
                elif self.retstatus == "0":
                    raise WSApiError(self.retmessage)

        except requests.Timeout:
            self.retstatus = '0'
            self.retmessage = "Request Timeout"
            self.retvalues = ''

        except requests.exceptions.HTTPError as e:
            raise e





if __name__ == "__main__":
    """
        Test run with test config data as supplied from Mike
    """

    key = 'dVm8cLOJ8AxiIX9dH0zmngQF4Oeevyd00fCcNaeu+jc=' # FROM SETTINGS
    ws = WSAPI(key)

    ws.device_id = '513C42C8ECCDAE79D15201AD53EDB97809F3D973' # FROM SETTINGS (DO I NEED A NEW ONE)
    #ws.url = '/api/v1/sn/getpn' # FROM SETTINGS 
    ws.url = '/api/v1/sn/biobanksubmission' # FROM SETTINGS 
    #ws.url = '/api/v1/sn/get_participant' # FROM SETTINGS 
    ws.host = 'https://ws-external-dev.genomicsmed.ie' # FROM SETTINGS
    ws.useragent = 'PYTHON CONNECTOR'
    ws.application = 'Python Test' # FROM SETTINGS
    ws.userid='mike.lacey' # DO I NEED A NEW ONE?

    #ws.params={'id':''}
    # TEST SUBMISSION
    """ws.params={"json_data": json.dumps({
           "samples": [
               {
                   "study_id": "HFT",
                   "site_id": "HBT",
                   "study_sub_cat": None,
                   "collab_id":"COLHT1",
                   "tube_id": "DNA1233333cor",
                   "sex": "male",
                   "sample_type": "DNA",
                   "volume" : "200",
                   "concentration" : "20"
               },
               {
                   "study_id": "HFT",
                   "site_id": "HBT",
                   "study_sub_cat": None,
                   "collab_id":"COLHT2",
                   "tube_id": "DNA1234444cor",
                   "sex": "female",
                   "sample_type": "DNA",
                   "volume" : "200",
                   "concentration" : "20"
               }
           ]
       })
    }"""
    # TEST RESULTS
    """
    [{
        'collab_id': 'COLHT1', 
        'tube_id': 'DNA1233333cor', 
        'ipn': '', 
        'repeat': '0', 
        'gt_id': 'GT1002339201', 
        'gd_id': 'GD1002339201', 
        'gr_id': 'GR1002339201', 
        'iPN': 'iPN8E45882', 
        'message': 'OK'
      }, 
      {
        'collab_id': 'COLHT2', 
        'tube_id': 'DNA1234444cor', 
        'ipn': '', 
        'repeat': '0', 
        'gt_id': 'GT1002339301', 
        'gd_id': 'GD1002339301', 
        'gr_id': 'GR1002339301', 
        'iPN': 'iPNADFBE54', 
        'message': 'OK'}
    ]

    """
    #ws.params = {'id':'iPN9AAEB6E'}

    # Recovering collab ID
    ws.params={"json_data": json.dumps([{'cid':'COLHT1'}])}
    ws.url = '/api/v1/sn/linkcid'
    
    ws.call_api()
    print(ws.retmessage)

    print(ws.retvalues)

    print(ws.retstatus)
