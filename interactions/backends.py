"""
    Custom LDAP Authentication backend.

    This is very simplistic, but unfortunately all the Django LDAP authentication backends
    are either far too opinionated or not up to date.

    This will do.
"""
import ldap3
import logging
from django.contrib.auth.backends import BaseBackend
from django.contrib.auth import get_user_model
from django.conf import settings
from interactions.models import LDAPUser

logger = logging.getLogger("auth_ldap")
User = get_user_model()


class LDAPBackend(BaseBackend):
    """
        Authenticate directly based on "AD\\<username>" and "<password>"
        
        Authentication involves:
            1. Direct Bind to AD
            2. Searching user groups foe the bound user
            3. Populating a User object
            4. Returning True
    """

    def __init__(self):
        """
            Not making anything fancy here
        """
        self.server = ldap3.Server(settings.AUTH_LDAP_HOST)
        self.connection = None

    def authenticate(self, request=None, username=None, password=None):
        if not username or username == "":
            return None
        if not password or password == "":
            return None

        user_is_bound = self.bind_ldap_user(username, password)
        if not user_is_bound:
            logger.debug("Authentication failed for {}".format(username))
            return None
        else:
            ldap_user = self.fetch_user_data(username)
            return self.populate_user_object(ldap_user)

        return None

    def get_user(self, user_id):
        """
        """
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None

    def bind_ldap_user(self, username, password):
        """
            Do a direct bind to the server.
        """
        try:
            self.connection = ldap3.Connection(
                self.server,
                read_only=True,
                user="AD\\%s" % username,
                password=password,
                authentication=ldap3.SIMPLE,
                auto_bind=True,
            )
        except ldap3.core.exceptions.LDAPBindError as e:
            return None
        except Exception as e:
            raise
        self.connection.bind()

        return True

    def fetch_user_data(self, username):
        """
            Do a search and extract User data from AD
        """
        self.search_filter = "({}={})".format(settings.AUTH_LDAP_UID_ATTRIB, username)
        entry = None
    
        self.connection.search(
            settings.AUTH_LDAP_BASE_DN,
            self.search_filter,
            #attributes=ldap3.ALL_ATTRIBUTES,
            attributes=["mail","memberof", "givenName", "sAMAccountName", "displayName", "objectGUID"]
        )
        entry = None
        for datum in self.connection.response:
            if datum['type'] == 'searchResEntry':
                entry = datum['attributes']
                break
        return entry
        

    def populate_user_object(self, entry):
        """
            Populate a User object from the AD Data
        """
        created, user = LDAPUser.objects.update_or_create_user(**entry)
        return user    
