from .base import *

SECRET_KEY = 'TOP_SECRET'

# I'm using Localstack for development. 
# TODO alternative dev setup
USING_LOCALSTACK = os.environ.get('USING_LOCALSTACK', "False").lower() == "true"
if USING_LOCALSTACK:
    AWS_S3_ENDPOINT_URL = "http://localhost:4572"
